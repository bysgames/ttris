#ifndef BYS_GAMES_TTRIS_DIALOG_HPP
#define BYS_GAMES_TTRIS_DIALOG_HPP

#include <BYS/UI/Container.hpp>
using namespace bys;

class Dialog : public Container
{
public:
    Dialog(const sf::String &title, const sf::String &icon, Scene *scene);
    void hide();
    void show();

    Signal <> onHideFinished;

private:
    Action *hideDialogAction;
    Action *showDialogAction;
};

#endif // BYS_GAMES_TTRIS_DIALOG_HPP
