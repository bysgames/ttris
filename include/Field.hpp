#ifndef BYS_GAMES_TTRIS_FIELD_HPP
#define BYS_GAMES_TTRIS_FIELD_HPP

#include "ttris.hpp"
#include "Piece.hpp"
#include <BYS/Game/Scene.hpp>
#include <BYS/TileMap/TileMap.hpp>
#include <SFML/Audio/Sound.hpp>
using namespace bys;

class Field : public TileMap
{
public:

    //////////////////////////////////////
    /// Constructs the field
    //////////////////////////////////////
    Field(Scene *scene);

    void restart();

    //////////////////////////////////////
    /// Updates the field, first checks if
    /// a new piece is required, if the
    /// piece can be inserted, then game over.
    /// Updates the Field::currentTime and
    /// if it's less than 0 moves the piece
    /// to down and reset Field::currentTime
    /// Field::stepTime
    //////////////////////////////////////
    void onUpdate(float deltaTime) override;

    void setPaused(bool paused);

    //////////////////////////////////////
    /// Moves (or at least tries it) the
    /// current piece to a given direction.
    /// It's called every Field::frameTime,
    /// on hard drop or when the user moves
    /// the piece.
    /// Returns if the piece could be moved
    /// or not.
    //////////////////////////////////////
    bool move(Direction direction);

    //////////////////////////////////////
    /// Drops the current piece (i.e., the
    /// current piece is positioned on
    /// droppedPiece position). It's called
    /// by the user.
    //////////////////////////////////////
    void drop();

    //////////////////////////////////////
    /// Tries to rotate the currentPiece.
    //////////////////////////////////////
    void rotate();

    void setDropPositionVisible(bool visible);

    int getScore() const;

    Signal <int> onGameOver;
    Signal <int> onGameWin;

private:
    //////////////////////////////////////
    /// Inserts a piece to the field. The piece
    /// if obtained from "nextPieceArea" child.
    /// Returns if the piece can be inserted
    /// or not.
    /// Is called once the piece can be moved
    /// to down.
    //////////////////////////////////////
    bool insertPiece();

    //////////////////////////////////////
    /// Clears all the filled rows and increase
    /// the score. It's called before to
    /// insert a new piece.
    //////////////////////////////////////
    void checkForFilledRows();

    //////////////////////////////////////
    /// Increase the score by a given amount
    /// and updates the score text.
    //////////////////////////////////////
    void increaseScore(int points);

    //////////////////////////////////////
    /// Increase the score by a given number
    /// of cleared rows. It's called by
    /// clearFiledRows
    //////////////////////////////////////
    void increaseScoreForClearedRows();

    //////////////////////////////////////
    /// Updates the score text
    //////////////////////////////////////
    void updateScore();

    //////////////////////////////////////
    /// Updates the drop position of the
    /// current piece (i.e., where the
    /// current piece will fail if the user
    /// performs a hard drop). It's called
    /// when the current piece is moved
    /// to left, to right or when it's
    /// rotated.
    //////////////////////////////////////
    void updateDropPosition();

    struct ClearingRowsInfo
    {
        ClearingRowsInfo();

        int rowCount;
        int rowsIndex [4];
        int columnIndex;
        float frameTime;
        float currentFrameTime;
    };

    bool paused;
    bool clearingRows;
    ClearingRowsInfo clearingRowsInfo;

    bool dropPositionVisible;

    int rowCount;           ///< field's row count
    int columnCount;        ///< field's column count
    bool needNewPiece;      ///< Tells if a new piece is needed

    float stepTime;         ///< Time for automatically move down the current piece, decreases as the level increases
    float currentStepTime;  ///< Current time, when it's 0, it's value is set as stepTime

    UInt score;             ///< Stores the score of the player
    int level;              ///< Current level, increase when currentClearedRows is equal than rowsRequiredForNextLevel
    int rowsRequiredForNextLevel;   ///< Determines how many rows must be cleared to reach the next level
    int currentClearedRows;         ///< Determine the current number of cleared rows

    Piece currentPiece;     ///< Current piece
    Piece droppedPiece;     ///< Current piece's drop position
    Layer *logicLayer;      ///< Has all the squares position
    Layer *dropPositionLayer;   ///< Has the squares for droppedPiece position

    sf::Sound dropSound;    ///< Played sound when the user drops the current piece

    Action *addScoreAction;
};

#endif // BYS_GAMES_TTRIS_FIELD_HPP
