#ifndef BYS_GAMES_TTRIS_GAME_SCENE_HPP
#define BYS_GAMES_TTRIS_GAME_SCENE_HPP

#include <BYS/Game/Scene.hpp>
using namespace bys;

class GameScene : public Scene
{
public:
    GameScene(Director &director);
    virtual void onClose(int scene = 0);
    void togglePause();

private:
    void restart();
    void gameOver(int score);
    bool isHighScore(int score);
    void registerHighScore();

    bool paused;
};

#endif // BYS_GAMES_TTRIS_GAME_SCENE_HPP
