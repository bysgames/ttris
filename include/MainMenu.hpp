#ifndef BYS_GAMES_TTRIS_MAIN_MENU_HPP
#define BYS_GAMES_TTRIS_MAIN_MENU_HPP

#include <BYS/Game/Scene.hpp>
using namespace bys;

class MainMenu : public Scene
{
public:
    MainMenu(Director &director);
    virtual void onClose(int scene = 0);
};

#endif // BYS_GAMES_TTRIS_MAIN_MENU_HPP
