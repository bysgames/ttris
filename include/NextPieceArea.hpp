#ifndef BYS_GAMES_TTRIS_NEXT_PIECE_AREA_HPP
#define BYS_GAMES_TTRIS_NEXT_PIECE_AREA_HPP

#include "Piece.hpp"
#include <BYS/TileMap/TileMap.hpp>
using namespace bys;

class NextPieceArea : public TileMap
{
public:
    NextPieceArea();
    const Piece & getPiece() const;
    void generatePiece();

private:
    Piece piece;
    Layer *pieceLayer;
};

#endif // BYS_GAMES_TTRIS_NEXT_PIECE_AREA_HPP
