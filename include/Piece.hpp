#ifndef BYS_GAMES_TTRIS_PIECE_HPP
#define BYS_GAMES_TTRIS_PIECE_HPP

#include "ttris.hpp"
#include <BYS/Core/Vector2D.hpp>
using namespace bys;

typedef unsigned int UInt;

class Piece
{
public:
    enum Type
    {
        O = 2,
        I = 3,
        J = 4,
        L = 5,
        S = 6,
        Z = 7,
        T = 8
    };

    Piece (Type type = O);

    void setType(Type type);
    Type getType() const;

    void move(Direction direction);
    Piece rotate();

    int getMaxY() const;
    int getMinY() const;

    const Vector2D & operator[](UInt index) const;
    Vector2D & operator[](UInt index);

private:
    void rotateSquare(int index);

    Type type;
    Vector2D squares [4];
};

#endif // BYS_GAMES_TTRIS_PIECE_HPP
