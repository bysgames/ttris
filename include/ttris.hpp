#ifndef BYS_GAMES_TTRIS_HPP
#define BYS_GAMES_TTRIS_HPP

enum ttris
{
    Exit = -1,
    Home = 0,
    Game = 1
};

enum Direction
{
    Down,
    Left,
    Right
};

#endif // BYS_GAMES_TTRIS_DIRECTION_HPP
