#include "Dialog.hpp"
#include <BYS/Actions/MoveTo.hpp>
#include <BYS/Game/Sprite.hpp>
#include <BYS/Game/Text.hpp>
#include <BYS/UI/Button.hpp>
#include <BYS/UI/HBoxContainer.hpp>
#include <BYS/UI/VBoxContainer.hpp>

//////////////////////////////////////////////
Dialog::Dialog(const sf::String &title, const sf::String &iconTexture, Scene *scene) : Container(scene)
{
    setModal(true);

    auto background = new Sprite("textures/dialogBackground.png");
    const auto &backgroundSize = background->getSize();
    addChild(background);

    auto titleText = new Text(title, "fonts/happy_monkey/HappyMonkey-Regular.ttf", 40);
    titleText->setColor(Color("white"));
    titleText->setBold(true);
    titleText->setOriginCenter();
    titleText->setPosition(0.f, backgroundSize.y * -0.5f + titleText->getSize().y * 0.5f + 30);
    addChild(titleText);

    auto icon = new Sprite(iconTexture);
    icon->setPosition(((backgroundSize * -0.5f) + Vector2D(10, 10) + icon->getSize()).toVector2f());
    addChild(icon);

    auto closeButton = new Button;
    closeButton->setName("closeButton");
    closeButton->setTexture("textures/miniQuitButton.png");
    closeButton->setPosition(backgroundSize.x * 0.5f - closeButton->getChild("spriteComponent")->getSize().x * 1.5f,
                             backgroundSize.y * -0.5f + 45);
    closeButton->onClick.connect(std::bind(&Dialog::hide, this));
    addChild(closeButton);

    //// Content
    auto content = new VBoxContainer;
    content->setName("content");
    addChild(content);

    /// Actions
    hideDialogAction = new MoveTo(0.8, Vector2D(1366.f / 2.f, 768.f + getFullBoundingBox().height * 0.5f), this);
    hideDialogAction->onFinish.connect(std::bind(&Signal<>::emit, &onHideFinished));
    hideDialogAction->onFinish.connect(std::bind(&Widget::loseFocus, this));
    showDialogAction = new MoveTo(0.8, Vector2D(1366.f / 2.f, 768.f / 2.f), this);
    setPosition(1366.f / 2.f, 768.f + getFullBoundingBox().height * 0.5f);
}

//////////////////////////////////////////////
void Dialog::hide()
{
    loseFocus();
    showDialogAction->stop();
    hideDialogAction->play();
}

//////////////////////////////////////////////
void Dialog::show()
{
    setFocus();
    hideDialogAction->stop();
    showDialogAction->play();
}
