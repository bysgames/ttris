#include "Field.hpp"
#include "NextPieceArea.hpp"
#include <BYS/Game/Text.hpp>
#include <BYS/Actions.hpp>
#include <BYS/UI/ProgressBar.hpp>

//////////////////////////////////////////////
Field::ClearingRowsInfo::ClearingRowsInfo() : rowCount(0), frameTime(0.03), currentFrameTime(0), columnIndex(0)
{
}

//////////////////////////////////////////////
Field::Field(Scene *scene) : needNewPiece(true), rowCount(22), columnCount(16), stepTime(1), currentStepTime(stepTime * 0.5f),
                             score(0), level(1), rowsRequiredForNextLevel(4), currentClearedRows(0), paused(false),
                             clearingRows(false), dropPositionVisible(true)
{
    setTileSize(Vector2D(32, 32));
    setTileset("textures/defaultTileset.png");
    setTilesAsDefault();
    setRowCount(rowCount);
    setColumnCount(columnCount);

    auto backgroundLayer = addLayer();
    for (int column = 0; column < columnCount; column++)
        for (int row = 0; row < rowCount; row++)
            (*backgroundLayer) [column] [row] = 1;

    logicLayer = addLayer("logicLayer");
    dropPositionLayer = addLayer("dropPosition");
    setOriginCenter();

    //// Next piece area
    auto nextPieceText = new Text("Next piece", "fonts/happy_monkey/HappyMonkey-Regular.ttf", 40);
    nextPieceText->setPosition(columnCount * 32 + 10, 0);
    nextPieceText->setColor(Color("white"));
    addChild(nextPieceText);

    auto *nextPieceArea = new NextPieceArea;
    nextPieceArea->setName("nextPieceArea");
    nextPieceArea->setPosition(columnCount * 32 + nextPieceArea->getSize().x * 0.5f + 20,
                               nextPieceArea->getSize().y * 0.5f + nextPieceText->getSize().y + 5);
    addChild(nextPieceArea);

    /// Game session info
    auto scoreText = new Text("Score:\n0", "fonts/happy_monkey/HappyMonkey-Regular.ttf", 40);
    scoreText->setPosition(columnCount * 32 + 10, (rowCount / 2 + rowCount / 4) * 32);
    scoreText->setColor(Color("white"));
    scoreText->setName("score");
    addChild(scoreText);

    auto levelText = new Text("Level:\n1", "fonts/happy_monkey/HappyMonkey-Regular.ttf", 40);
    levelText->setPosition(columnCount * 32 + 10, (rowCount / 2) * 32);
    levelText->setColor(Color("white"));
    levelText->setName("level");
    addChild(levelText);

    auto progressLevel = new ProgressBar(scene, ProgressBar::Vertical);
    progressLevel->setCSS("css/Field.css", "progressLevel");
    progressLevel->setPosition(- (progressLevel->getSize().x * 0.5f + 50.f), (rowCount / 2) * 32);
    progressLevel->setMaxValue(4);
    progressLevel->setValue(0);
    addChild(progressLevel);

    /// Added score text
    auto addedScoreText = new Text("", "fonts/happy_monkey/HappyMonkey-Regular.ttf", 40);
    addedScoreText->setName("addedScore");
    addedScoreText->setVisible(false);
    addedScoreText->setColor(Color("white"));
    addChild(addedScoreText);

    addScoreAction = new MoveBy(0.5f, Vector2D(10, 0), addedScoreText);
    addScoreAction->onFinish.connect(std::bind(&Entity::setVisible, addedScoreText, false));

    dropSound.setBuffer(*AssetManager::getSoundBuffer("sounds/drop.ogg"));
}

//////////////////////////////////////////////
void Field::restart()
{
    score = 0;
    updateScore();

    needNewPiece = true;
    currentStepTime = stepTime = 1;

    auto progressLevel = static_cast <ProgressBar *> (getChild("progressLevel"));
    progressLevel->setMaxValue(4);
    progressLevel->setValue(0);

    rowsRequiredForNextLevel = 4;
    currentClearedRows = 0;

    level = 1;
    static_cast <Text *> (getChild("level"))->setString("Level:\n1");

    logicLayer->clear();
    dropPositionLayer->clear();
}

//////////////////////////////////////////////
void Field::onUpdate(float deltaTime)
{
    if (paused)
        return;

    if (clearingRows)
    {
        dropPositionLayer->setVisible(false);
        clearingRowsInfo.currentFrameTime += deltaTime;
        if (clearingRowsInfo.currentFrameTime < clearingRowsInfo.frameTime)
            return;

        clearingRowsInfo.currentFrameTime = 0;
        if (clearingRowsInfo.columnIndex < columnCount)
        {
            for (int i = 0; i < clearingRowsInfo.rowCount; i++)
                (*logicLayer) [clearingRowsInfo.columnIndex] [clearingRowsInfo.rowsIndex [i]] = 0;

            clearingRowsInfo.columnIndex++;
            return;
        }

        /// Actually, clear the filled rows
        for (int i = 0; i < clearingRowsInfo.rowCount; i++)
        {
            for (int row = clearingRowsInfo.rowsIndex [i] + i; row >= 0; row--)
            {
                for (int column = 0; column < columnCount; column++)
                {
                    if (row)
                    {
                        (*logicLayer) [column] [row] = (*logicLayer) [column] [row - 1];
                        (*logicLayer) [column] [row - 1] = 0;
                    }
                    else
                        (*logicLayer) [column] [row] = 0;
                }
            }
        }

        increaseScoreForClearedRows();
        clearingRows = false;
        clearingRowsInfo.rowCount = 0;
        clearingRowsInfo.currentFrameTime = 0;
        clearingRowsInfo.columnIndex = 0;
        needNewPiece = true;
        dropPositionLayer->setVisible(dropPositionVisible);
    }

    if (needNewPiece && ! insertPiece())
    {
        onGameOver.emit(score);
        return;
    }

    currentStepTime -= deltaTime;
    if (currentStepTime > 0)
        return;

    currentStepTime = stepTime;

    move(Down);
}

//////////////////////////////////////////////
void Field::setPaused(bool paused)
{
    this->paused = paused;
}

//////////////////////////////////////////////
bool Field::insertPiece()
{
    auto nextPieceArea = static_cast <NextPieceArea *> (getChild("nextPieceArea"));
    currentPiece = nextPieceArea->getPiece();
    nextPieceArea->generatePiece();
    needNewPiece = false;

    if (currentPiece.getMinY() < 0)
    {
        int minY = currentPiece.getMinY();
        for (int i = 0; i < 4; i++)
            currentPiece [i].y += minY * - 1.f;
    }

    for (int i = 0; i < 4; i++)
    {
        currentPiece [i].x += columnCount / 2 - 1;

        /// Test if the piece can be inserted
        const auto &square = currentPiece [i];
        if ((*logicLayer) [(int) square.x] [(int) square.y])
            return false;

        (*logicLayer) [square.x] [square.y] = currentPiece.getType();
    }

    updateDropPosition();
    return true;
}

//////////////////////////////////////////////
void Field::checkForFilledRows()
{
    for (int row = rowCount - 1; row >= 0; row--)
    {
        bool isFilled = true;
        bool isEmpty = true;
        for (int column = 0; column < columnCount; column++)
        {
            if (! (*logicLayer) [column] [row])
                isFilled = false;
            else
                isEmpty = false;
        }

        /// If the row is empty, then there's no more rows with blocks for checking
        if (isEmpty)
        {
            if (clearingRowsInfo.rowCount)
                clearingRows = true;
            else
                needNewPiece = true;

            return;
        }

        if (isFilled)
        {
            clearingRowsInfo.rowCount++;
            clearingRowsInfo.rowsIndex [clearingRowsInfo.rowCount - 1] = row;
        }
    }

    needNewPiece = true;
}

//////////////////////////////////////////////
void Field::increaseScore(int points)
{
    if (! points)
        return;

    score += points;
    updateScore();

    auto addedScore = static_cast <Text *> (getChild("addedScore"));
    auto scoreText = getChild("score");
    addedScore->setString("+" + std::to_string(points));
    addedScore->setVisible(true);
    scoreText->getSize();
    addedScore->setPosition(scoreText->getPosition().x + 10 + scoreText->getSize().x,
                            scoreText->getPosition().y + scoreText->getSize().y * 0.5f);
    addScoreAction->play();
}

//////////////////////////////////////////////
void Field::increaseScoreForClearedRows()
{
    if (! clearingRowsInfo.rowCount)
        return;

    currentClearedRows += clearingRowsInfo.rowCount;
    int points = 0;

    switch (clearingRowsInfo.rowCount)
    {
        case 1:  points += 40;   break;
        case 2:  points += 100;  break;
        case 3:  points += 300;  break;
        case 4:  points = 1200;
    }

    auto progressLevel = static_cast <ProgressBar *> (getChild("progressLevel"));

    if (currentClearedRows >= rowsRequiredForNextLevel) /// Level up!!
    {
        points += level * 1000;
        currentClearedRows -= rowsRequiredForNextLevel;
        rowsRequiredForNextLevel += 4;
        level++;
        static_cast <Text *> (getChild("level"))->setString("Level:\n" + std::to_string(level));
        progressLevel->setMaxValue(rowsRequiredForNextLevel);
        stepTime -= 0.1f;
    }

    progressLevel->setValue(currentClearedRows);
    increaseScore(points);
    updateScore();

    if (level == 11)
        onGameWin.emit(score);
}

//////////////////////////////////////////////
int Field::getScore() const
{
    return score;
}

//////////////////////////////////////////////
void Field::updateScore()
{
    static_cast <Text *> (getChild("score"))->setString("Score:\n" + std::to_string(score));
}

//////////////////////////////////////////////
bool Field::move(Direction direction)
{
    if (clearingRows || paused)
        return false;

    //// Clear current piece
    for (int i = 0; i < 4; i++)
        (*logicLayer) [currentPiece [i].x] [currentPiece [i].y] = 0;

    //// Determines if the currentPiece can be moved
    Piece movedPiece = currentPiece;
    movedPiece.move(direction);
    bool canBeMoved = true;

    for (int i = 0; i < 4; i++)
    {
        int y = movedPiece [i].y, x = movedPiece [i].x;
        if (y < 0 || y >= rowCount || x < 0 || x >= columnCount || (*logicLayer) [x] [y])
        {
            canBeMoved = false;
            break;
        }
    }

    if (! canBeMoved)
    {
        //// Reinsert currentPiece
        for (int i = 0; i < 4; i++)
            (*logicLayer) [currentPiece [i].x] [currentPiece [i].y] = currentPiece.getType();

        if (direction == Down)
            checkForFilledRows();

        return canBeMoved;
    }

    //// Insert movedPiece
    for (int i = 0; i < 4; i++)
        (*logicLayer) [movedPiece [i].x] [movedPiece [i].y] = movedPiece.getType();

    currentPiece.move(direction);
    if (direction != Down)
        updateDropPosition();
    return canBeMoved;
}

//////////////////////////////////////////////
void Field::rotate()
{
    if (clearingRows || paused)
        return;

    //// Clear current piece
    for (int i = 1; i < 4; i++)
        (*logicLayer) [currentPiece [i].x] [currentPiece [i].y] = 0;

    //// Determines if the currentPiece can be rotated
    const auto &rotatedPiece = currentPiece.rotate();
    bool canBeRotated = true;

    for (int i = 1; i < 4; i++)
    {
        int y = rotatedPiece [i].y, x = rotatedPiece [i].x;
        if (y < 0 || y >= rowCount || x < 0 || x >= columnCount || (*logicLayer) [x] [y])
        {
            canBeRotated = false;
            break;
        }
    }

    if (! canBeRotated)
    {
        //// Reinsert currentPiece
        for (int i = 1; i < 4; i++)
            (*logicLayer) [currentPiece [i].x] [currentPiece [i].y] = currentPiece.getType();
        return;
    }

    //// Insert movedPiece
    for (int i = 1; i < 4; i++)
        (*logicLayer) [rotatedPiece [i].x] [rotatedPiece [i].y] = rotatedPiece.getType();

    currentPiece = rotatedPiece;
    updateDropPosition();
}

//////////////////////////////////////////////
void Field::setDropPositionVisible(bool visible)
{
    if (visible == dropPositionVisible)
        return;

    dropPositionVisible = visible;
    dropPositionLayer->setVisible(visible);
}

//////////////////////////////////////////////
void Field::drop()
{
    if (clearingRows || paused)
        return;

    increaseScore((droppedPiece.getMaxY() - currentPiece.getMaxY()) * ((dropPositionVisible) ? 1 : 2));

    /// Clear the current piece and insert it on droppedPiece position
    for (int i = 0; i < 4; i++)
    {
        (*logicLayer) [currentPiece [i].x] [currentPiece [i].y] = 0;
        (*logicLayer) [droppedPiece [i].x] [droppedPiece [i].y] = currentPiece.getType();
    }

    dropSound.play();
    checkForFilledRows();
}

//////////////////////////////////////////////
void Field::updateDropPosition()
{
    /// Remove droppedPiece and currentPiece
    for (int i = 0; i < 4; i++)
    {
        (*dropPositionLayer) [droppedPiece [i].x] [droppedPiece [i].y] = 0;
        (*logicLayer) [currentPiece [i].x] [currentPiece [i].y] = 0;
    }

    /// Update dropped piece
    droppedPiece = currentPiece;

    bool droppedPieceCanBeMoved = true;
    while (droppedPieceCanBeMoved)
    {
        droppedPiece.move(Down);
        if (droppedPiece.getMaxY() >= rowCount)
        {
            for (int i = 0; i < 4; i++)
                droppedPiece [i].y -= 1;
            droppedPieceCanBeMoved = false;
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                if ((*logicLayer) [droppedPiece [i].x] [droppedPiece [i].y])
                {
                    for (int i = 0; i < 4; i++)
                        droppedPiece [i].y -= 1;
                    droppedPieceCanBeMoved = false;
                }
            }
        }
    }

    /// Reinsert droppedPiece and currentPiece
    for (int i = 0; i < 4; i++)
    {
        (*dropPositionLayer) [droppedPiece [i].x] [droppedPiece [i].y] = 9;
        (*logicLayer) [currentPiece [i].x] [currentPiece [i].y] = currentPiece.getType();
    }
}
