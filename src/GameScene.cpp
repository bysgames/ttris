#include "GameScene.hpp"
#include "Field.hpp"
#include "Dialog.hpp"
#include "ttris.hpp"
#include <BYS/Core/Logger.hpp>
#include <BYS/UI/Button.hpp>
#include <BYS/UI/TextField.hpp>
#include <BYS/UI/HBoxContainer.hpp>
#include <BYS/UI/VBoxContainer.hpp>
#include <sqlite3.h>

//////////////////////////////////////////////
GameScene::GameScene(Director &director) : Scene(director), paused(false)
{
    setBackgroundColor(Color("#171c1c"));
    setMouseVisible(false);

    auto field = new Field(this);
    field->setName("field");
    field->setPosition(683, 384);
    field->onGameOver.connect(std::bind(&GameScene::gameOver, this, std::placeholders::_1));
    addChild(field);

    connectToKeyboard(sf::Keyboard::P, std::bind(&GameScene::togglePause, this));
    connectToKeyboard(sf::Keyboard::D, std::bind(&Field::move, field, Right));
    connectToKeyboard(sf::Keyboard::A, std::bind(&Field::move, field, Left));
    connectToKeyboard(sf::Keyboard::S, std::bind(&Field::move, field, Down));
    connectToKeyboard(sf::Keyboard::R, std::bind(&Field::rotate, field));
    connectToKeyboard(sf::Keyboard::Space, std::bind(&Field::drop, field));

    /// Pause dialog
    auto pauseDialog = new Dialog("Pause", "", this);
    pauseDialog->setName("pauseDialog");
    pauseDialog->onHideFinished.connect(std::bind(&Field::setPaused, field, false));
    pauseDialog->onHideFinished.connect(std::bind(&Scene::setMouseVisible, this, false));
    addChild(pauseDialog);

    auto pauseDialogContent = static_cast <VBoxContainer *> (pauseDialog->getChild("content"));
    pauseDialogContent->setMargin(50);

    auto button = new Button;
    button->setCSS("css/GameScene.css", "greenButton");
    button->setString("resume");
    button->onClick.connect(std::bind(&GameScene::togglePause, this));
    pauseDialogContent->addChild(button);

    button = new Button;
    button->setCSS("css/GameScene.css", "toggleButtonOn");
    button->setString("Drop position");
    button->setChecked(true);
    button->onToggle.connect([button, field](bool toggle)
                             {  button->setCSSClassName((toggle) ? "toggleButtonOn" : "toggleButtonOff");
                                field->setDropPositionVisible(toggle);  });
    pauseDialogContent->addChild(button);

    auto hboxContainer = new HBoxContainer(nullptr, 30);
    pauseDialogContent->addChild(hboxContainer);

    button = new Button;
    button->setCSS("css/GameScene.css", "homeButton");
    button->onClick.connect(std::bind(&Scene::close, this, ttris::Home));
    hboxContainer->addChild(button);

    button = new Button;
    button->setCSS("css/GameScene.css", "restartButton");
    button->onClick.connect(std::bind(&GameScene::restart, this));
    hboxContainer->addChild(button);

    button = new Button;
    button->setCSS("css/GameScene.css", "exitButton");
    button->onClick.connect(std::bind(&Scene::close, this, ttris::Exit));
    hboxContainer->addChild(button);

    /// Game over dialog
    auto gameOverDialog = new Dialog("Game Over", "", this);
    gameOverDialog->setName("gameOverDialog");
    gameOverDialog->onHideFinished.connect(std::bind(&Scene::setMouseVisible, this, false));
    gameOverDialog->onHideFinished.connect(std::bind(&Field::setPaused, field, false));
    gameOverDialog->getChild("closeButton")->setVisible(false);
    addChild(gameOverDialog);

    auto gameOverDialogContent = static_cast <VBoxContainer *> (gameOverDialog->getChild("content"));
    gameOverDialogContent->setMargin(100);

    auto text = new Text("", "fonts/happy_monkey/HappyMonkey-Regular.ttf", 50);
    text->setName("score");
    text->setColor(Color("white"));
    gameOverDialogContent->addChild(text);

    hboxContainer = new HBoxContainer(nullptr, 30);
    gameOverDialogContent->addChild(hboxContainer);

    button = new Button;
    button->setCSS("css/GameScene.css", "homeButton");
    button->onClick.connect(std::bind(&Scene::close, this, ttris::Home));
    hboxContainer->addChild(button);

    button = new Button;
    button->setCSS("css/GameScene.css", "restartButton");
    button->onClick.connect(std::bind(&GameScene::restart, this));
    hboxContainer->addChild(button);

    button = new Button;
    button->setCSS("css/GameScene.css", "exitButton");
    button->onClick.connect(std::bind(&Scene::close, this, ttris::Exit));
    hboxContainer->addChild(button);

    ///// Enter name for new high score dialog
    auto enterNameDialog = new Dialog("Highscore!!!", "", this);
    enterNameDialog->setName("enterNameDialog");
    enterNameDialog->onHideFinished.connect(std::bind(&Dialog::show, gameOverDialog));
    enterNameDialog->onFocusOut.connect(std::bind(printf, "eND: focusOut\n"));
    addChild(enterNameDialog);

    auto enterNameDialogContent = enterNameDialog->getChild("content");

    text = new Text("Enter your name:", "fonts/happy_monkey/HappyMonkey-Regular.ttf", 30);
    text->setColor(Color("white"));
    text->setOriginCenter();
    enterNameDialogContent->addChild(text);

    auto textField = new TextField(nullptr, this);
    textField->setCSS("css/GameScene.css", "textField");
    textField->onFocus.connect(std::bind(printf, "focus\n"));
    textField->onFocusOut.connect(std::bind(printf, "focusOut\n"));
    enterNameDialogContent->addChild(textField);

    button = new Button;
    button->setCSS("css/GameScene.css", "greenButton");
    button->setString("Ok");
    button->onClick.connect(std::bind(&GameScene::registerHighScore, this));
    enterNameDialogContent->addChild(button);
}

//////////////////////////////////////////////
#include "MainMenu.hpp"
void GameScene::onClose(int scene)
{
    switch (scene)
    {
        case ttris::Exit: directorAction.type = DirectorAction::Exit; break;

        case ttris::Home:
            directorAction.type = DirectorAction::PopPushScene;
            directorAction.scene = new MainMenu(*getDirector());
    }
}

//////////////////////////////////////////////
void GameScene::togglePause()
{
    paused = ! paused;

    if (paused)
    {
        setMouseVisible(true);
        static_cast <Dialog *> (getChild("pauseDialog"))->show();
        static_cast <Field *> (getChild("field"))->setPaused(paused);
    }
    else
        static_cast <Dialog *> (getChild("pauseDialog"))->hide();
}

//////////////////////////////////////////////
void GameScene::restart()
{
    static_cast <Field *> (getChild("field"))->restart();
    if (paused)
        togglePause();
    else
        static_cast <Dialog *> (getChild("gameOverDialog"))->hide();
}

//////////////////////////////////////////////
void GameScene::gameOver(int score)
{
    setMouseVisible(true);
    static_cast <Field *> (getChild("field"))->setPaused(true);

    if (isHighScore(score))
    {
        auto enterNameDialog = static_cast <Dialog *> (getChild("enterNameDialog"));
        enterNameDialog->show();
        return;
    }

    auto gameOverDialog = static_cast <Dialog *> (getChild("gameOverDialog"));
    auto scoreText = static_cast <Text *> (gameOverDialog->getChild("content")->getChild("score"));
    scoreText->setString("Score: " + std::to_string(score));
    scoreText->setOriginCenter();
    gameOverDialog->show();
}

//////////////////////////////////////////////
bool GameScene::isHighScore(int score)
{
    sqlite3 *db;
    sqlite3_stmt *stmt;
    char *errorMsg = nullptr;
    int result = sqlite3_open((AssetManager::assetsPath + "ttris.sql").toAnsiString().c_str(), &db);

    if (result)
    {
        Logger::print("GameScene", "Can't open database: ?", StringList() << sqlite3_errmsg(db));
        sqlite3_close(db);
        return false;
    }

    /// Check if the records table exists
    result = sqlite3_prepare_v2(db, "SELECT COUNT(*) FROM sqlite_master WHERE type='table' AND name='high_scores'", -1, &stmt,
                                nullptr);
    if (result != SQLITE_OK)
    {
        Logger::print("GameScene", "Error preparing stmt: ?", StringList() << sqlite3_errmsg(db));
        sqlite3_close(db);
        return false;
    }

    sqlite3_step(stmt);
    if (! sqlite3_column_int(stmt, 0)) /// if don't exist, then create it
    {
        Logger::print("GameScene", "Creating high_scores table...");
        sqlite3_finalize(stmt);

        result = sqlite3_prepare_v2(db, "CREATE TABLE high_scores (name TEXT, score INT)", -1, &stmt, nullptr);
        if (result != SQLITE_OK)
        {
            Logger::print("GameScene", "Error creating high_scores table: ?", StringList() << sqlite3_errmsg(db));
            sqlite3_close(db);
            return false;
        }

        sqlite3_step(stmt);
        sqlite3_finalize(stmt);
        sqlite3_close(db);
        return true; /// It's high score because it's the unique registered score xD
    }
    sqlite3_finalize(stmt);

    ///// Get current high scores
    result = sqlite3_prepare_v2(db, sf::String("SELECT COUNT(*) FROM high_scores WHERE score <= " +
                                    std::to_string(score)).toAnsiString().c_str(), -1, &stmt, nullptr);

    if (result != SQLITE_OK)
    {
        Logger::print("GameScene", "Error getting high scores: ?", StringList() << sqlite3_errmsg(db));
        sqlite3_close(db);
        return false;
    }

    sqlite3_step(stmt);
    bool highScore = sqlite3_column_int(stmt, 0);
    sqlite3_finalize(stmt);
    sqlite3_close(db);

    return highScore;
}

//////////////////////////////////////////////
void GameScene::registerHighScore()
{
    const auto &score = std::to_string(static_cast <Field *> (getChild("field"))->getScore());
    auto enterNameDialog = static_cast <Dialog *> (getChild("enterNameDialog"));
    const auto &name = static_cast <TextField *> (enterNameDialog->getChild("content")->getChild("textField"))->getString();

    sqlite3 *db;
    sqlite3_stmt *stmt;
    int result = sqlite3_open((AssetManager::assetsPath + "ttris.sql").toAnsiString().c_str(), &db);

    if (result)
    {
        Logger::print("GameScene", "Can't open database: ?", StringList() << sqlite3_errmsg(db));
        sqlite3_close(db);
        return;
    }

    const auto &query = sf::String(sf::String("INSERT INTO high_scores VALUES ('") + name + "', " + score + ")");
    result = sqlite3_prepare_v2(db, query.toAnsiString().c_str(), -1, &stmt, nullptr);

    if (result != SQLITE_OK)
    {
        Logger::print("GameScene", "Error preparing stmt: ?", StringList() << sqlite3_errmsg(db));
        sqlite3_close(db);
        return;
    }

    sqlite3_step(stmt);
    sqlite3_finalize(stmt);
    sqlite3_close(db);
    enterNameDialog->hide();
    auto scoreText = static_cast <Text *> (getChild("gameOverDialog")->getChild("content")->getChild("score"));
    scoreText->setString("Score: " + score);
    scoreText->setOriginCenter();
}
