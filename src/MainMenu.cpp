#include "MainMenu.hpp"
#include "Dialog.hpp"
#include "ttris.hpp"
#include <BYS/Actions/RotateBy.hpp>
#include <BYS/UI/Button.hpp>

//////////////////////////////////////////////
MainMenu::MainMenu(Director &director) : Scene(director)
{
    setName("mainMenu");
    setBackgroundColor(Color("#171c1c"));

    auto title = new Text("ttris", "fonts/edit_undo/edundot.ttf", 150);
    title->setColor(Color("white"));
    title->setOriginCenter();
    title->setPosition(683, 120);
    addChild(title);

    /////////////////// options dialog
    auto optionsDialog = new Dialog("Settings", "textures/optionsIcon.png", this);
    auto optionsDialogContent = optionsDialog->getChild("content");

    auto button = new Button;
    button->setTexture("textures/soundOn.png");
    button->setChecked(true);
    button->onToggle.connect([button](bool toggle)
                             { button->setTexture((toggle) ? "textures/soundOn.png" : "textures/soundOff.png", true); });
    optionsDialogContent->addChild(button);

    button = new Button;
    button->setCSS("css/Dialogs.css", "saveSettingsButton");
    button->onClick.connect(std::bind(&Dialog::hide, optionsDialog));
    optionsDialogContent->addChild(button);

    optionsDialog->setZIndex(2);
    addChild(optionsDialog);

    /////////////////// Main buttons
    auto *playButton = new Button(this);
    playButton->setCSS("css/MainMenu.css", "playButton");
    addChild(playButton);
    playButton->onClick.connect(std::bind(&Scene::close, this, ttris::Game));

    auto optionsButton = new Button(this);
    optionsButton->setCSS("css/MainMenu.css", "optionsButton");
    auto rotateOptionsButton = new RotateBy(TimeAction::Infinite, 200, false, optionsButton);
    optionsButton->onMouseIn.connect(std::bind(&Action::play, rotateOptionsButton));
    optionsButton->onMouseOut.connect(std::bind(&Action::stop, rotateOptionsButton));
    optionsButton->onClick.connect(std::bind(&Action::stop, rotateOptionsButton));
    optionsButton->onClick.connect(std::bind(&Dialog::show, optionsDialog));
    addChild(optionsButton);

    auto quitButton = new Button(this);
    quitButton->setCSS("css/MainMenu.css", "quitButton");
    quitButton->onClick.connect(std::bind(&Scene::close, this, ttris::Exit));
    addChild(quitButton);
}

//////////////////////////////////////////////
#include "GameScene.hpp"
void MainMenu::onClose(int scene)
{
    switch (scene)
    {
        case ttris::Exit:  directorAction.type = DirectorAction::Exit;  break;

        case ttris::Game:
            directorAction.type = DirectorAction::PopPushScene;
            directorAction.scene = new GameScene(*getDirector());
        break;
    }
}
