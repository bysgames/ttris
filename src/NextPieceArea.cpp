#include "NextPieceArea.hpp"

//////////////////////////////////////////////
NextPieceArea::NextPieceArea()
{
    int columnCount = 5;
    int rowCount = 5;

    setTileSize(Vector2D(32, 32));
    setTileset("textures/defaultTileset.png");
    setTilesAsDefault();
    setRowCount(rowCount);
    setColumnCount(columnCount);

    auto backgroundLayer = addLayer();
    for (int column = 0; column < columnCount; column++)
        for (int row = 0; row < rowCount; row++)
            (*backgroundLayer) [column] [row] = 1;

    pieceLayer = addLayer();
    setOriginCenter();
}

//////////////////////////////////////////////
const Piece & NextPieceArea::getPiece() const
{
    return piece;
}

//////////////////////////////////////////////
void NextPieceArea::generatePiece()
{
    /// Clear piece
    auto pieceType = piece.getType();
    int offsetX = 2;
    int offsetY = (pieceType == Piece::I || pieceType == Piece::J || pieceType == Piece::L) ? 1 : 2;
    for (int i = 0; i < 4; i++)
        (*pieceLayer) [piece [i].x + offsetX] [piece [i].y + offsetY] = 0;

    /// Generate the next piece
    pieceType = Piece::Type(rand() % 7 + 2);
    piece.setType(pieceType);
    int rotationsNumber = rand() % 4;
    for (int i = 0; i < rotationsNumber; i++)
        piece = piece.rotate();

    /// Reinsert it
    offsetY = (pieceType == Piece::I || pieceType == Piece::J || pieceType == Piece::L) ? 1 : 2;
    for (int i = 0; i < 4; i++)
        (*pieceLayer) [piece [i].x + offsetX] [piece [i].y + offsetY] = pieceType;
}
