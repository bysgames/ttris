#include "Piece.hpp"

//////////////////////////////////////////////
Piece::Piece(Type type)
{
    setType(type);
}

//////////////////////////////////////////////
void Piece::setType (Type type)
{
    this->type = type;
    switch (type)
    {
        case O:
            squares [0].x = 0;  squares [0].y = 0;
            squares [1].x = 1;  squares [1].y = 0;
            squares [2].x = 0;  squares [2].y = 1;
            squares [3].x = 1;  squares [3].y = 1;
        break;

        case I:
            squares [0].x = 0;  squares [0].y = 1;
            squares [1].x = 0;  squares [1].y = 0;
            squares [2].x = 0;  squares [2].y = 2;
            squares [3].x = 0;  squares [3].y = 3;
        break;

        case J:
            squares [0].x = 0;   squares [0].y = 1;
            squares [1].x = 0;   squares [1].y = 0;
            squares [2].x = 0;   squares [2].y = 2;
            squares [3].x = -1;  squares [3].y = 2;
        break;

        case L:
            squares [0].x = 0;  squares [0].y = 1;
            squares [1].x = 0;  squares [1].y = 0;
            squares [2].x = 0;  squares [2].y = 2;
            squares [3].x = 1;  squares [3].y = 2;
        break;

        case S:
            squares [0].x = 0;   squares [0].y = 0;
            squares [1].x = 1;   squares [1].y = 0;
            squares [2].x = 0;   squares [2].y = 1;
            squares [3].x = -1;  squares [3].y = 1;
        break;

        case Z:
            squares [0].x = 0;   squares [0].y = 0;
            squares [1].x = -1;  squares [1].y = 0;
            squares [2].x = 0;   squares [2].y = 1;
            squares [3].x = 1;  squares [3].y = 1;
        break;

        case T:
            squares [0].x = 0;   squares [0].y = 0;
            squares [1].x = -1;  squares [1].y = 0;
            squares [2].x = 1;   squares [2].y = 0;
            squares [3].x = 0;   squares [3].y = 1;
        break;
    }
}

//////////////////////////////////////////////
Piece::Type Piece::getType() const
{
    return type;
}

//////////////////////////////////////////////
void Piece::move(Direction direction)
{
    switch (direction)
    {
        case Down:
            for (int i = 0; i < 4; i++)
                squares [i].y += 1;
        break;

        case Right:
            for (int i = 0; i < 4; i++)
                squares [i].x += 1;
        break;

        case Left:
            for (int i = 0; i < 4; i++)
                squares [i].x -= 1;
    }
}

//////////////////////////////////////////////
int Piece::getMaxY() const
{
    int maxY = squares [0].y;
    for (int i = 1; i < 4; i++)
        maxY = std::max(maxY, (int) squares [i].y);

    return maxY;
}

//////////////////////////////////////////////
int Piece::getMinY() const
{
    int minY = squares [0].y;
    for (int i = 1; i < 4; i++)
        minY = std::min(minY, (int) squares [i].y);

    return minY;
}

//////////////////////////////////////////////
Piece Piece::rotate()
{
    Piece rotatedPiece = *this;

    if (type == O)
        return rotatedPiece;

    for (int i = 1; i < 4; i++)
        rotatedPiece.rotateSquare(i);

    return rotatedPiece;
}

//////////////////////////////////////////////
void Piece::rotateSquare(int index)
{
    if (index <= 0 || index >= 4)
        return;

    Vector2D &squareToRotate = squares [index];
    const auto &origin = squares [0];
    if (squareToRotate.y > origin.y)
    {
        if (squareToRotate.x < origin.x)
            squareToRotate.y -= 2;
        else if (squareToRotate.x == origin.x)
        {
            if (index == 3 && type == I)
            {
                squareToRotate.y -= 2;
                squareToRotate.x -= 2;
            }
            else
            {
                squareToRotate.y -= 1;
                squareToRotate.x -= 1;
            }
        }
        else if (squareToRotate.x > origin.x)
            squareToRotate.x -= 2;
    }
    else if (squareToRotate.y == origin.y)
    {
        if (squareToRotate.x < origin.x)
        {
            if (index == 3 && type == I)
            {
                squareToRotate.y -= 2;
                squareToRotate.x += 2;
            }
            else
            {
                squareToRotate.y -= 1;
                squareToRotate.x += 1;
            }
        }
        else
        {
            if (index == 3 && type == I)
            {
                squareToRotate.y += 2;
                squareToRotate.x -= 2;
            }
            else
            {
                squareToRotate.y += 1;
                squareToRotate.x -= 1;
            }
        }
    }
    else if (squareToRotate.y < origin.y)
    {
        if (squareToRotate.x < origin.x)
            squareToRotate.x += 2;
        else if (squareToRotate.x == origin.x)
        {
            if (index == 3 && type == I)
            {
                squareToRotate.y += 2;
                squareToRotate.x += 2;
            }
            else
            {
                squareToRotate.y += 1;
                squareToRotate.x += 1;
            }
        }
        else if (squareToRotate.x > origin.x)
            squareToRotate.y += 2;
    }
}

//////////////////////////////////////////////
const Vector2D & Piece::operator[](UInt index) const
{
    return squares [index];
}

//////////////////////////////////////////////
Vector2D & Piece::operator[](UInt index)
{
    return squares [index];
}
