#include "Assets.h"
#include "MainMenu.hpp"
#include <cstdlib>
#include <BYS/Game/AssetManager.hpp>
#include <BYS/Game/Director.hpp>
using namespace bys;

int main()
{
    srand(time(nullptr));
    AssetManager::assetsPath = ASSETS;
    auto &director = Director::getInstance();
    director.create(sf::VideoMode::getDesktopMode(), "ttris", sf::Style::Fullscreen);
    director.setFramerateLimit(60);

    return director.run(new MainMenu(director));
}
